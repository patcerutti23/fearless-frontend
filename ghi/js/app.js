function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card shadow-lg p-25 mb-5  bg-body rounded"">
        <img src=${pictureUrl} class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">${starts} - ${ends}</div>
        </div>
      </div>
    `;
}

function alert() {
  return `
  <div class="alert alert-primary" role="alert">
  An error has occured. Please refresh page.
  </div>
  `
}




window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // console.log('api fetch unsuccessful')
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.title;
              const name = details.conference.name
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const starts = new Date(details.conference.starts).toLocaleDateString();
              const ends = new Date(details.conference.ends).toLocaleDateString();
              const location = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, starts, ends, location);
              const columns = document.querySelectorAll('.col');
              columns[data.conferences.indexOf(conference) % 3].innerHTML += html;
            }
          }

        }
        //to help see javascript errors
    } catch (e) {
      console.error(e);
      console.log("Errors in Javascript")
    }

  });






    // You click on the link that has Response.json() and see that the return value is a Promise object.
    // You update your code to get the value from the JSON-formatted data that is in the response.
    // You make sure to type the await keyword because it's a Promise that is being returned.
    // You don't want the Promise. You want the value the Promise will turn into after you await it
