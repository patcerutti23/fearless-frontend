window.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('login-form');
    form.addEventListener('submit', async event => {
      event.preventDefault(); //default is to save data with the url, prevents the page from refreshing

    // const data = Object.fromEntries(new FormData(form))
      const fetchOptions = {
        method: 'post',
        body: new FormData(form),
        credential: 'include',
        // headers: {
        //   'Content-Type': 'application/json',
        // }
      };
      const url = 'http://localhost:8000/login/';
      const response = await fetch(url, fetchOptions);
      if (response.ok) {
        window.location.href = '/';

        document.getElementsByID('location').classList.remove('d-none');
      } else {
        console.error(response);
      }
    });
  });
