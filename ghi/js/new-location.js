window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/"
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        for (let state of data.states) {
            const stateBox = document.getElementById('state'); //returns element object representing the id specified 'state'
            const option = document.createElement('option'); //creates an HTML element specified by tagName 'option' in this case in our html
            option.value = state.abbreviation; //this is what we see in the backend
            option.innerHTML = state.name; //this is what the user sees in the dropdownbox
            stateBox.appendChild(option); //"Child" is referring to looping this child html tag <option selected value="">Choose a state</option>
        }
    }
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault(); //prevents browser sending back to server, and refreshing the whole page, we want to send bact to our RESTful API
        const formData = new FormData(formTag); //FormData object lets you compile a set of key/value pairs to send using XMLHttpRequest
        const json = JSON.stringify(Object.fromEntries(formData)); //converts form submit data to json 17-18
        const locationUrl = 'http://localhost:8000/api/locations/'; //sends data to the server
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset(); //resets the form to the original state when a user clicks submit
        const newLocation = await response.json();
        }
    });
});
